$( document ).ready(function() {
  wgbl = {

    init: function() {
      this.bindUIActions();
    },

    bindUIActions: function(){
      this.backtoTop();
      this.menuActions();
      this.lazyLoad();
    },

    menuActions: function(){
      if($("#wgbl-nav").length > 0)
      {
        var self = this;
        $("#menu").click(function(){
          $("#wgbl-nav").css("height","100%");
        });

        $(".closebtn").click(function(){
          $("#wgbl-nav").css("height","0%");
        })
      }
    },

    lazyLoad: function(){
      if($(".lazy").length > 0)
      {
        $('.lazy').lazy({
            delay: 5000
        });
      }
    },

    backtoTop: function(){

      $(window).scroll(function(){
        if ($(this).scrollTop() > 250)
        {
          $('.backtop').fadeIn();
        }
        else
        {
          $('.backtop').fadeOut();
        }
      });

      $('.backtop').click(function(){
        $('html, body').animate({scrollTop : 0},800);
        return false;
      });
    }


  };

  wgbl.init();
});